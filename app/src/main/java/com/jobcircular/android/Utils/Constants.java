package com.jobcircular.android.Utils;

public class Constants {
   public static final String BASE_URL="https://www.job-circular.com/app/admin/";
  // public static final String BASE_URL="https://bd-alo.com/jobs/admin/";
   //  public static final String BASE_URL="http://192.168.1.11/jobbuzz/";


    public static final String APP_INFO=BASE_URL+"api.php?app_details";
    public static final String CATE_LIST="api.php?cat_list";
    public static final String IAMGE_FILE_PATH=BASE_URL+"/images/";
    public static final String FILE_PATH=BASE_URL+"/uploads/";
    public static final String BASE_API="api.php?";
    public static final String HOME_API="api.php?home";

    /*This code for app info shareed preference */
    public static final String APP_NFO="app_info";
    public static final String APP_NAME="app_name";
    public static final String APP_LOGO="app_logo";
    public static final String APP_VERSION="app_version";
    public static final String APP_AUTHOR="app_author";
    public static final String APP_CONTACT="app_contact";
    public static final String APP_EMAIL="app_email";
    public static final String APP_WEBSITE="app_website";
    public static final String APP_DESCRIPTION="app_description";
    public static final String APP_PRIVACY_POLICY="app_privacy_policy";
    /*------------------------------------------------------------------*/

    public static int KEY_DATA_HOME=1;




   public static int pos=999;

}
