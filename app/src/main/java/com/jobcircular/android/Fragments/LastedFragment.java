package com.jobcircular.android.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jobcircular.android.Adapter.CircularAdapter;
import com.jobcircular.android.ApiService.APIClient;
import com.jobcircular.android.ApiService.APIInterface;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.circular.Circular;
import com.jobcircular.android.Models.featured.AppCircular;
import com.jobcircular.android.Models.featured.FeaturedCircular;
import com.jobcircular.android.Models.featured.HomeCircular;
import com.jobcircular.android.R;

import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LastedFragment extends Fragment {


    RecyclerView circularList;

    LinearLayout contentLayout;

    LinearLayout failed_layout;
    TextView failed_message;
    Button failed_retry;
    LinearLayout progressBar;

    APIInterface apiInterface;
    SwipeRefreshLayout swipeRefreshLayout;

    public static final String TAG=FeaturedFragment.class.getSimpleName();
    int key=1;

    public LastedFragment() {
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_featured, container, false);
        ButterKnife.bind(getActivity(),view);
        apiInterface= APIClient.getClient().create(APIInterface.class);



        initAction();



        return view;
    }

    private void initAction(){
        initUi();

        if (App.isInternetOn(getActivity())){
            initCircularView();
        }else {
            inintErrorLayout(View.VISIBLE,getString(R.string.error_internet));
        }
    }


    private void initUi(){
        circularList=view.findViewById(R.id.circularList);
        contentLayout=view.findViewById(R.id.contentLayout);
        failed_layout=view.findViewById(R.id.failed_layout);
        failed_message=view.findViewById(R.id.failed_message);
        failed_retry=view.findViewById(R.id.failed_retry);
        progressBar=view.findViewById(R.id.progressBarLayout);
        swipeRefreshLayout=view.findViewById(R.id.swiperefresh);


        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (App.isInternetOn(getActivity())){
                            initCircularView();
                        }else {
                            inintErrorLayout(View.VISIBLE,getString(R.string.error_internet));
                        }
                    }
                }
        );
    }


    private void initCircularView() {


        inintErrorLayout(View.GONE,"");
        progressBar.setVisibility(View.VISIBLE);

        Call<AppCircular> call=apiInterface.getHomeCircular();
        call.enqueue(new Callback<AppCircular>() {
            @Override
            public void onResponse(Call<AppCircular> call, Response<AppCircular> response) {

                if (response.isSuccessful()){
                    List<Circular> circular_List=response.body().getAPP().getLatestCirculars();
                    if (circular_List.size() !=0){
                        initJobCircularView(circular_List);
                        inintErrorLayout(View.GONE,"");
                        progressBar.setVisibility(View.GONE);
                    }else {
                        inintErrorLayout(View.VISIBLE,getString(R.string.error_nodata));
                    }
                }

            }

            @Override
            public void onFailure(Call<AppCircular> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                // Toasty.warning(ActivityJobsCategory.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                inintErrorLayout(View.VISIBLE,getString(R.string.error_server));
            }
        });

    }


    private void inintErrorLayout(int visivility,String  msg){
        switch (visivility){
            case View.GONE:
                failed_layout.setVisibility(View.GONE);
                contentLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                break;
            case View.VISIBLE:
                failed_layout.setVisibility(View.VISIBLE);
                contentLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                break;
        }
        if (msg !=null){
            failed_message.setText(msg);
            failed_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (App.isInternetOn(getActivity())){
                        initCircularView();
                    }else {
                        inintErrorLayout(View.VISIBLE,getString(R.string.error_internet));
                    }
                }
            });
        }
    }

    private void initJobCircularView(List<Circular> data) {
        Log.d(TAG, "initJobCircularView: lasted "+data.size());
        swipeRefreshLayout.setRefreshing(false);
        CircularAdapter adapter=new CircularAdapter(getActivity(),data);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        circularList.setLayoutManager(manager);
        circularList.setHasFixedSize(true);
        circularList.setAdapter(adapter);
    }

}
