
package com.jobcircular.android.Models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobsCategory {

    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("total_circulars")
    @Expose
    private String totalCirculars;


    public boolean isTextChanged;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTotalCirculars() {
        return totalCirculars;
    }

    public void setTotalCirculars(String totalCirculars) {
        this.totalCirculars = totalCirculars;
    }

    public void setTextChanged(boolean imageChanged) {
        isTextChanged = imageChanged;
    }
    public boolean isTextChanged() {
        return isTextChanged;
    }

}
