
package com.jobcircular.android.Models.featured;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppCircular {

    @SerializedName("APP")
    @Expose
    private HomeCircular aPP;

    public HomeCircular getAPP() {
        return aPP;
    }

    public void setAPP(HomeCircular aPP) {
        this.aPP = aPP;
    }

}
