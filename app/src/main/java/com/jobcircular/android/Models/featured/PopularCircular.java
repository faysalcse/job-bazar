
package com.jobcircular.android.Models.featured;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PopularCircular {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("aid")
    @Expose
    private String aid;
    @SerializedName("circular_title")
    @Expose
    private String circularTitle;
    @SerializedName("circular_description")
    @Expose
    private String circularDescription;
    @SerializedName("circular_cover_img")
    @Expose
    private String circularCoverImg;
    @SerializedName("circular_bg_img")
    @Expose
    private String circularBgImg;
    @SerializedName("circular_file_type")
    @Expose
    private String circularFileType;
    @SerializedName("circular_file_url")
    @Expose
    private String circularFileUrl;
    @SerializedName("total_rate")
    @Expose
    private String totalRate;
    @SerializedName("rate_avg")
    @Expose
    private String rateAvg;
    @SerializedName("circular_views")
    @Expose
    private String circularViews;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    private String subcategoryName;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("postdate")
    @Expose
    private String postdate;
    @SerializedName("name_of_position")
    @Expose
    private String nameOfPosition;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getCircularTitle() {
        return circularTitle;
    }

    public void setCircularTitle(String circularTitle) {
        this.circularTitle = circularTitle;
    }

    public String getCircularDescription() {
        return circularDescription;
    }

    public void setCircularDescription(String circularDescription) {
        this.circularDescription = circularDescription;
    }

    public String getCircularCoverImg() {
        return circularCoverImg;
    }

    public void setCircularCoverImg(String circularCoverImg) {
        this.circularCoverImg = circularCoverImg;
    }

    public String getCircularBgImg() {
        return circularBgImg;
    }

    public void setCircularBgImg(String circularBgImg) {
        this.circularBgImg = circularBgImg;
    }

    public String getCircularFileType() {
        return circularFileType;
    }

    public void setCircularFileType(String circularFileType) {
        this.circularFileType = circularFileType;
    }

    public String getCircularFileUrl() {
        return circularFileUrl;
    }

    public void setCircularFileUrl(String circularFileUrl) {
        this.circularFileUrl = circularFileUrl;
    }

    public String getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(String totalRate) {
        this.totalRate = totalRate;
    }

    public String getRateAvg() {
        return rateAvg;
    }

    public void setRateAvg(String rateAvg) {
        this.rateAvg = rateAvg;
    }

    public String getCircularViews() {
        return circularViews;
    }

    public void setCircularViews(String circularViews) {
        this.circularViews = circularViews;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getNameOfPosition() {
        return nameOfPosition;
    }

    public void setNameOfPosition(String nameOfPosition) {
        this.nameOfPosition = nameOfPosition;
    }

}
