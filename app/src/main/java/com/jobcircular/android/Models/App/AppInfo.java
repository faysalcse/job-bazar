
package com.jobcircular.android.Models.App;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppInfo {

    @SerializedName("APP")
    @Expose
    private List<APP> aPP = null;

    public List<APP> getAPP() {
        return aPP;
    }

    public void setAPP(List<APP> aPP) {
        this.aPP = aPP;
    }

}
