package com.jobcircular.android.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.jobcircular.android.Fragments.FeaturedFragment;
import com.jobcircular.android.Fragments.LastedFragment;
import com.jobcircular.android.Fragments.PopularFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new LastedFragment();
        }
        else if (position == 1)
        {
            fragment = new LastedFragment();
        }
        else if (position == 2)
        {
            fragment = new PopularFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "সাম্প্রতিক";
        }
        else if (position == 1)
        {
            title = "সর্বশেষ";
        }
        else if (position == 2)
        {
            title = "জনপ্রিয়";
        }
        return title;
    }
}