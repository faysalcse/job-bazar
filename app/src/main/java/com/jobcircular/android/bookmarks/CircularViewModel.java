package com.jobcircular.android.bookmarks;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;


public class CircularViewModel extends AndroidViewModel {

    public static final String TAG=CircularViewModel.class.getSimpleName();
    CircularDao circularDao;
    AppDatabase database;
    Context context;

    private LiveData<List<Circular>> allBookmarkCircular;
    private Circular circularById;


    public CircularViewModel(@NonNull Application application) {
        super(application);
        context=application;
        database= AppDatabase.getInstance(application);
        circularDao=database.circularDao();
        allBookmarkCircular=circularDao.loadAllCirculars();


    }

    public boolean insert(Circular circular){
        new InsertAsyncTask(circularDao).execute(circular);
        return InsertAsyncTask.handleResult();

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "onCleared: View model destroyed");
    }

    public LiveData<List<Circular>> getAllBookmarkCircular(){
        return allBookmarkCircular;
    }

    public Boolean gerCircularByAid(int circular_id){
        circularById=circularDao.loadCircularById(circular_id);
        if (circularById !=null){
            return true;
        }
        return false;
    }

    private static class InsertAsyncTask extends AsyncTask<Circular, Void, Long> {

        CircularDao circularDao;
        static boolean results;

        public InsertAsyncTask(CircularDao circularDao) {
            this.circularDao=circularDao;
        }

        @Override
        protected Long doInBackground(Circular... circulars) {
            long result = circularDao.insertCircular(circulars[0]);
            Log.d(TAG, "doInBackground: result" + result);
            if (result != -1) {
                results = true;
                return result;
            } else {
                results = false;
                return result;
            }
        }


        public static boolean handleResult(){
            return results;
        }

        @Override
        protected void onPostExecute(Long aVoid) {
            if (aVoid != -1) {
                results = true;
            } else {
                results = false;
            }

        }
    }


}
