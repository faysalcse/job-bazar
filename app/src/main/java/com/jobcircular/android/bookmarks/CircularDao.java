package com.jobcircular.android.bookmarks;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CircularDao {

    @Query("SELECT * FROM Circular ORDER BY ID")
    LiveData<List<Circular>> loadAllCirculars();

    @Insert
    long insertCircular(Circular circular);

    @Update
    void updateCircular(Circular circular);

    @Delete
    void delete(Circular circular);

    @Query("DELETE FROM Circular")
    public void deleteAllBookMarkCircular();

    @Query("SELECT * FROM Circular WHERE circularId = :id")
    Circular loadCircularById(int id);
}