package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jobcircular.android.ApiService.APIClient;
import com.jobcircular.android.ApiService.APIInterface;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.circular_details.CircularDetails;
import com.jobcircular.android.Models.circular_details.EJOBSAPP;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Constants;
import com.jobcircular.android.Utils.Tools;
import com.jobcircular.android.bookmarks.AppDatabase;
import com.jobcircular.android.bookmarks.Circular;
import com.jobcircular.android.bookmarks.CircularDao;
import com.jobcircular.android.bookmarks.CircularViewModel;
import com.jobcircular.android.downloader.DirectoryHelper;
import com.jobcircular.android.downloader.DownloadPDFService;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;


import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivityCircularDetails extends AppCompatActivity {


    Toolbar toolbar;
    APIInterface apiInterface;
    Context context;

    @BindView(R.id.circular_title)
    TextView circular_title;

     @BindView(R.id.circular_position)
    TextView circular_position;

     @BindView(R.id.circular_deadline)
    TextView circular_deadline;

     @BindView(R.id.circular_totalviews)
    TextView circular_totalviews;

    HtmlTextView circular_descripitions;

     @BindView(R.id.circularImage)
     PhotoView circularImage;

     @BindView(R.id.download_btn)
     Button download_btn;


     @BindView(R.id.loadingLayout)
     LinearLayout loadingLayout;

     @BindView(R.id.circularAttachImageLayout)
     LinearLayout circularAttachImageLayout;

     @BindView(R.id.contentLayout)
     FrameLayout contentLayout;

     @BindView(R.id.actionLayout)
     FrameLayout actionLayout;


     String circularAid;
    List<EJOBSAPP> circularDetails;

    private String filename;
    String finalDownloadFileUrl;
    CircularViewModel circularViewModel;

    CircularDao circularDao;

    Menu menu;


    public static final String TAG=ActivityCircularDetails.class.getSimpleName();

    DownloadManager dManager;
    TextView tvMessage;
    long did;
    boolean isBookmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular_details);
        ButterKnife.bind(this);
        context=this;
        apiInterface= APIClient.getClient().create(APIInterface.class);
        circularViewModel= ViewModelProviders.of(this).get(CircularViewModel.class);
        circularDao = AppDatabase.getInstance(getApplicationContext()).circularDao();


        Intent intent=getIntent();

        if (intent !=null){
            circularAid=intent.getStringExtra("cir_id");
        }

        initToolbar();

        if (App.isInternetOn(context)){
            intiCircularDetails(circularAid);
        }else {
            Toasty.normal(context,getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
            loadingLayout.setVisibility(View.GONE);
        }

    }

    private void intiCircularDetails(String catId) {
        showLoading(View.VISIBLE);
        Log.d(TAG, "intiCircularDetails: "+catId);

        Call<CircularDetails> call=apiInterface.getSingleCircularDetailsById(catId);
        call.enqueue(new Callback<CircularDetails>() {
            @Override
            public void onResponse(Call<CircularDetails> call, Response<CircularDetails> response) {

                Log.d(TAG, "onResponse: "+response.body().getEJOBSAPP());

                if (response.isSuccessful()){
                    circularDetails=response.body().getEJOBSAPP();
                    if (circularDetails.size() !=0){
                        initUiData(circularDetails.get(0));
                        contentLayout.setVisibility(View.VISIBLE);
                    }else {
                        Toasty.warning(getApplicationContext(), getString(R.string.error_nodata), Toast.LENGTH_SHORT).show();
                        contentLayout.setVisibility(View.GONE);
                    }
                }

               loadingLayout.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CircularDetails> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                Toasty.warning(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                contentLayout.setVisibility(View.GONE);
                showLoading(View.GONE);
            }
        });

        new CheckBookmark(circularDao).execute(circularAid);

    }

    class CheckBookmark extends AsyncTask<String,Void,Void>{

        CircularDao circularDao;
        boolean results=false;

        public CheckBookmark(CircularDao circularDao) {
            this.circularDao=circularDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            Circular result = circularDao.loadCircularById(Integer.parseInt(strings[0]));
            Log.d(TAG, "doInBackground: result" + result);
            if (result != null) {
                results = true;
            } else {
                results = false;
            }
            isBookmark=results;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (results){
                if (menu !=null)
                menu.getItem(2).setIcon(ContextCompat.getDrawable(context, R.drawable.ic_bookmark_black_24dp));
            }
        }
    }


    private void initUiData(EJOBSAPP data) {

        circular_descripitions=(HtmlTextView)findViewById(R.id.circular_descripitions);

        Log.d(TAG, "initUiData: "+data.getCircularTitle());

        if (data.getCircularTitle().length() !=0 && !TextUtils.isEmpty(data.getCircularTitle())){
            circular_title.setText(getString(R.string.companyTItle)+" "+data.getCircularTitle().trim());
        }

        if (data.getNameOfPosition().length() !=0 && !TextUtils.isEmpty(data.getNameOfPosition())){
            circular_position.setText(getString(R.string.positionName)+" "+data.getNameOfPosition().trim());
        }else {
            circular_position.setVisibility(View.GONE);
        }

        if (data.getDeadline().length() !=0 && !TextUtils.isEmpty(data.getDeadline())){
            String replacedOne = data.getDeadline().replaceAll("0","০").replaceAll("1","১").replaceAll("2","২").replaceAll("3","৩").replaceAll("4","৪").replaceAll("5","৫").replaceAll("6","৬").replaceAll("7","৭").replaceAll("8","৮").replaceAll("9","৯");

            circular_deadline.setText(getString(R.string.deadline)+" "+App.reverseDate(replacedOne.trim()));
        }


        if (data.getCircularViews().length() !=0 && !TextUtils.isEmpty(data.getCircularViews())){
            String replacedOne = data.getCircularViews().replaceAll("0","০").replaceAll("1","১").replaceAll("2","২").replaceAll("3","৩").replaceAll("4","৪").replaceAll("5","৫").replaceAll("6","৬").replaceAll("7","৭").replaceAll("8","৮").replaceAll("9","৯");
            circular_totalviews.setText("সর্বমোট দেখা হয়েছেঃ "+replacedOne.trim()+" বার ");
        }



        if (data.getCircularDescription().length() !=0 && !TextUtils.isEmpty(data.getCircularDescription())){

                /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        circular_descripitions.setText(Html.fromHtml(data.getCircularDescription(),imgGetter,null));
                    } else {
                        circular_descripitions.setText(Html.fromHtml(data.getCircularDescription()));
                    }*/

          //  circular_descripitions.setHtml(data.getCircularDescription(),imgGetter);


            circular_descripitions.setHtml(data.getCircularDescription(),
                    new HtmlResImageGetter(context));


        }

        if (data.getCircularCoverImg().length() !=0 && !TextUtils.isEmpty(data.getCircularCoverImg()) && !data.getCircularCoverImg().contains("no_image")){

          /*  Glide.with(context)
                    .load(Constants.IAMGE_FILE_PATH+data.getCircularCoverImg())
                    .fitCenter()
                    .placeholder(R.drawable.ic_loading)
                    .into(circularImage);*/

            Picasso.with(context)
                    .load(Constants.IAMGE_FILE_PATH+data.getCircularCoverImg())
                    .fit()
                    .placeholder(R.drawable.ic_loading)
                    .into(circularImage);


        }else {
            circularAttachImageLayout.setVisibility(View.GONE);
        }



        Log.d(TAG, "File type : "+data.getCircularFileType()+" FIle url : "+data.getCircularFileUrl());


        if (!TextUtils.isEmpty(data.getCircularFileType()) && data.getCircularFileType() !=null ){

            if (data.getCircularFileUrl().length() != 0
                    && !TextUtils.isEmpty(data.getCircularFileUrl())
                    && data.getCircularFileType().length() != 0) {

                String fileType = data.getCircularFileType();
                String downloadFileUrl = null;

                if (fileType.equals("server_url")) {
                    downloadFileUrl = Constants.FILE_PATH + data.getCircularFileUrl();
                } else {
                    downloadFileUrl = data.getCircularFileUrl();
                }


                finalDownloadFileUrl = downloadFileUrl;
                download_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int MyVersion = Build.VERSION.SDK_INT;
                        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                            if (!checkIfAlreadyhavePermission()) {
                                requestForSpecificPermission();
                            } else {

                                if (finalDownloadFileUrl != null) {
                                    Toasty.normal(context, getString(R.string.downloading), Toasty.LENGTH_SHORT).show();
                                    startService(DownloadPDFService.getDownloadService(context, finalDownloadFileUrl, DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/")));
                                }

                            }
                        }
                    }
                });


            } else {
                actionLayout.setVisibility(View.GONE);
            }
        }else{
            actionLayout.setVisibility(View.GONE);
        }

        showLoading(View.GONE);

    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }



    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.title_menu_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menuItem) {
        getMenuInflater().inflate(R.menu.menu_circular_details, menuItem);
        menu=menuItem;
        return true;
    }

    public void showLoading(int visivility){
        if (visivility==View.GONE){
            contentLayout.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }

    private void shareAction(){
        EJOBSAPP data=circularDetails.get(0);
        String finalMessag="প্রতিষ্ঠানের নামঃ "+data.getCircularTitle()+" ।\n\n";

        if (data.getNameOfPosition() !=null && !TextUtils.isEmpty(data.getNameOfPosition()) ){
            finalMessag=finalMessag.concat("পদবীঃ "+data.getNameOfPosition()+" ।\n\n");
        }

        if (data.getDeadline() !=null && !TextUtils.isEmpty(data.getDeadline()) ){
            finalMessag=finalMessag.concat("আবেদনের শেষ তারিখঃ "+data.getDeadline()+"।\n\n");
        }

        if (finalDownloadFileUrl !=null && !TextUtils.isEmpty(finalDownloadFileUrl) ){
            finalMessag=finalMessag.concat("সার্কুলার ডাউনলোড ফাইলঃ "+finalDownloadFileUrl+"।\n\n");
        }

        finalMessag=finalMessag.concat(getString(R.string.app_marketing_text)+"https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName());

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, finalMessag);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }else if (item.getItemId()==R.id.action_share){
            shareAction();
        }else if (item.getItemId()==R.id.action_bookmark){
            if (isBookmark){
                Toast.makeText(context, getString(R.string.already_exists), Toast.LENGTH_SHORT).show();
            }else {
                addToBookmark();
            }


        }else if (item.getItemId()==R.id.action_refresh){
            if (App.isInternetOn(context)){
                intiCircularDetails(circularAid);
            }else {
                Toasty.normal(context,getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    private void addToBookmark(){
        if (circularDetails !=null){
            EJOBSAPP circular=circularDetails.get(0);

            String deatLine=circular.getDeadline();

            if (deatLine !=null && !TextUtils.isEmpty(deatLine)){
                deatLine=circular.getDeadline();
            }else {
                deatLine="null";
            }

            String positions;
            if (circular.getNameOfPosition() !=null && TextUtils.isEmpty(circular.getNameOfPosition())){
                positions=circular.getNameOfPosition();
            }else {
                positions=null;
            }



            Circular data=new Circular(
                    circular.getId(),
                    circular.getCircularTitle(),
                    positions,
                    circular.getSubcategoryName().toString(),
                    deatLine,
                    circular.getPostdate().toString()

            );

           boolean result =  circularViewModel.insert(data);

           if (result){
               Toast.makeText(context, getString(R.string.add_bookmark), Toast.LENGTH_SHORT).show();
               if (menu !=null)
                     menu.getItem(2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_bookmark_black_24dp));
           }else {
              // Toast.makeText(context, getString(R.string.faild_to_addbokmark), Toast.LENGTH_SHORT).show();
               if (menu !=null)
                   menu.getItem(2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_bookmark_border_black_24dp));
           }







        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "File permission granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "File permission not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }




}
