package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Tools;
import com.jobcircular.android.bookmarks.Circular;
import com.jobcircular.android.bookmarks.CircularAdapter;
import com.jobcircular.android.bookmarks.CircularViewModel;

import java.util.List;

public class BookMarkActivity extends AppCompatActivity {

    RecyclerView bookmarkCircular;
    CircularViewModel model;
    List<Circular> bookmarkCircularList;
    CircularAdapter bookmarkAdapter;

    Toolbar toolbar;
    LinearLayout warningLayout;
    LinearLayout contentLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_mark);

        initToolbar(getString(R.string.title_bookmark));

        model= ViewModelProviders.of(this).get(CircularViewModel.class);
        bookmarkAdapter=new CircularAdapter(this);
        bookmarkCircular=findViewById(R.id.circularList);
        warningLayout=findViewById(R.id.warningLayout);
        contentLayout=findViewById(R.id.contentLayout);
        model.getAllBookmarkCircular().observe(this, new Observer<List<Circular>>() {
            @Override
            public void onChanged(List<Circular> circulars) {

                if (circulars.size() !=0){
                    contentLayout.setVisibility(View.VISIBLE);
                    warningLayout.setVisibility(View.GONE);
                    bookmarkAdapter.setCirculars(circulars);
                    bookmarkCircular.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    bookmarkCircular.setAdapter(bookmarkAdapter);
                }else {
                    contentLayout.setVisibility(View.GONE);
                    warningLayout.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    private void initToolbar(String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
