package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jobcircular.android.ActivityDocs;
import com.jobcircular.android.Adapter.NavCategoryAdapter;
import com.jobcircular.android.Fragments.HomeFragment;
import com.jobcircular.android.Models.Category;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Constants;
import com.jobcircular.android.Utils.SharedPref;
import com.jobcircular.android.Utils.Tools;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    Context context;

    public static final String TAG="RetrofitOperations";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;

        initToolbar();
        initDrawerMenu();



    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorTextAction), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

    }

    private void initDrawerMenu() {
        NavigationView nav_view = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        TextView name = nav_view.findViewById(R.id.name);
        TextView settings = nav_view.findViewById(R.id.settings);
        ImageView avatar = nav_view.findViewById(R.id.avatar);
        RecyclerView subCatList = nav_view.findViewById(R.id.subCatList);


        if (new HomeFragment() !=null){

            actionBar.setTitle("হোম");
            drawer.closeDrawers();
            openFragment(new HomeFragment());
        }


        List<Category> list=new ArrayList<>();
        list.add(new Category("পরীক্ষার খবরঃ",true));
        list.add(new Category("2","নোটিশ বোর্ড",R.drawable.ic_notice_board,false));
        list.add(new Category("3","পরীক্ষার সময়সূচী",R.drawable.ic_exam_time,false));
        list.add(new Category("4","ফলাফল",R.drawable.ic_result,false));

        list.add(new Category("জব ক্যাটাগরিঃ",true));
        list.add(new Category("5","সরকারী",R.drawable.ic_sub_category,false));
        list.add(new Category("6","বেসরকারী",R.drawable.ic_sub_category,false));
        list.add(new Category("7","ডিফেন্স",R.drawable.ic_sub_category,false));
        list.add(new Category("8","মন্ত্রণালয়",R.drawable.ic_sub_category,false));
        list.add(new Category("9","সরকারী ব্যাংক",R.drawable.ic_sub_category,false));
        list.add(new Category("10","বেসরকারী ব্যাংক",R.drawable.ic_sub_category,false));
        list.add(new Category("11","শিক্ষক নিয়োগ",R.drawable.ic_sub_category,false));
        list.add(new Category("12","সিটি কর্পোরেশন",R.drawable.ic_sub_category,false));
        list.add(new Category("13","স্কুল ও কলেজ",R.drawable.ic_sub_category,false));
        list.add(new Category("14","বিশ্ববিদ্যালয়",R.drawable.ic_sub_category,false));
        list.add(new Category("15","রেল ও সড়ক",R.drawable.ic_sub_category,false));
        list.add(new Category("16","নদী ও বন্দর",R.drawable.ic_sub_category,false));
        list.add(new Category("17","সেলস/মার্কেটিং",R.drawable.ic_sub_category,false));
        list.add(new Category("18","মেডিকেল ও হসপিটাল",R.drawable.ic_sub_category,false));
        list.add(new Category("19","অন্যান্য",R.drawable.ic_sub_category,false));


        list.add(new Category("বিসিএস ও নিয়োগ পরীক্ষাঃ",true));
        list.add(new Category("20"," বিগত বিসিএস প্রশ্ন",R.drawable.ic_question_bces,false));
        list.add(new Category("21"," বিগত নিয়োগ প্রশ্ন",R.drawable.ic_question_bces,false));
        list.add(new Category("22","এনটিআরসিএ প্রশ্ন",R.drawable.ic_question_bces,false));

        list.add(new Category("জব প্রিপারেশনঃ",true));
        list.add(new Category("23","প্রস্তুতি",R.drawable.ic_preprations,false));
        list.add(new Category("24","উৎসাহ",R.drawable.ic_ustno,false));
        list.add(new Category("25","মডেল টেস্ট",R.drawable.ic_model_test,false));
        list.add(new Category("26","ইন্টারভিউ বোর্ড",R.drawable.ic_interview_board,false));

        list.add(new Category("অন্যান্য প্রয়োজনীয়ঃ",true));
        list.add(new Category("27","ফি ও আবেদন পত্র",R.drawable.ic_interview_board,false));
        list.add(new Category("29","জীবন বৃত্তান্ত (সিভি)",R.drawable.ic_interview_board,false));
        list.add(new Category("100","বয়স ক্যালকুলেটর",R.drawable.ic_calclutor,false));



        NavCategoryAdapter adapter=new NavCategoryAdapter(context,list);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        subCatList.setLayoutManager(llm);
        subCatList.setAdapter(adapter);







        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //  showInterstitial();
            }
        });
    }

    public void closeDrawer(){
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void onDrawerMenuClick(View view) {
        Fragment fragment = null;
        Fragment fragementHome=new HomeFragment();
        String title = actionBar.getTitle().toString();
        int menu_id = view.getId();
        switch (menu_id) {

            case R.id.nav_age_calculator:
                startActivity(new Intent(this, ActivityAgeCalculator.class));
                break;

            case R.id.nav_menu_about:
                startActivity(new Intent(this, ActivityAboutUS.class));
                break;

            case R.id.nav_menu_category:
//                startActivity(new Intent(this, ActivityJobsCategory.class));
                break;


            case R.id.nav_menu_bookmark:
                startActivity(new Intent(this, BookMarkActivity.class));
                break;

            case R.id.nav_menu_home:
                if (new HomeFragment() == null) {
                   /* finish();
                    startActivity(new Intent(this, MainActivity.class));*/
                    fragment=new HomeFragment();
                }
                fragment = fragementHome;
                title = getString(R.string.title_menu_home);
                break;


            case R.id.nav_privacy_policy:
                startActivity(new Intent(MainActivity.this, ActivityDocs.class).putExtra("message",SharedPref.getKey(context,Constants.APP_PRIVACY_POLICY)).putExtra("title",getString(R.string.title_privacy_policy)));
                break;
            case R.id.nav_menu_trams_conditions:

                String conditions="  এই অ্যাপ ব্যাবহার করার জন্য আপনাকে আমাদের শর্তাবলী মেনে নিতে হবে। যেমন\n" +
                        "    \n" +
                        "    1. আমাদের অ্যাপ্লিকেশনে প্রকাশিত সকল নিয়োগ বিজ্ঞপ্তি বিভিন্ন অফিসিয়াল ওয়েব সাইট ও নিউজ পোর্টাল থেকে সংগ্রহ করা হয়ে থাকে। তাই, আমরা কোন বিজ্ঞপ্তির সত্যতা যাচাই এর ক্ষমতা রাখি না।\n" +
                        "    \n" +
                        "    2. বিজ্ঞপ্তি দাতাদের সাথে সকল প্রকার আর্থিক লেনদেন করা থেকে সম্পূর্ণ আপনাদের বিরত থাকার অনুরোধ করা হলো। আমাদের অ্যাপ্লিকেশন এ প্রকাশিত কোন বিজ্ঞপ্তির মাধ্যমে আপনি প্রতারিত হলে আমরা দায়ী নয়। তাই বিশেষ ভাবে উল্লেখ করা হলো যে, নগদ অর্থ, সম্পদ, ব্যাংক চেক সহ সকল লেনদেন থেকে বিরত থাকবেন। যদি কোন প্রতিষ্ঠান বা ব্যক্তি চাকুরির প্রত্যাশা দিয়ে অর্থ দাবি করে তাহলে আইনি সহয়তা গ্রহণ করুন।\n" +
                        "    \n" +
                        "    3. চাকুরির বিজ্ঞপ্তিতে যে কোন কারনে আবেদন সময়সীমা, পরীক্ষার সময়, তারিখ এ স্থান পরিবর্তন হতে পারে। আপনি যে কোন কারনে আবেদন পত্র জমা, পরীক্ষার ফি জমা দেওয়া, প্রবেশ পত্র গ্রহন, পরীক্ষা সহ ইন্টারভিউ দিতে ব্যর্থ হলে আমরা দায়ী নয়।\n" +
                        "    \n" +
                        "    4. কিছু অসাধু বা চক্রান্তকারী দল বেকার ও চাকুরি প্রত্যাশিত ব্যক্তির মোবাইল নাম্বার, জন্ম তারিখ, ঠিকানা ও ইমেইল সহ বিভিন্ন তথ্য সংগ্রহ করে থাকে। তাই আপনার সকল ব্যক্তিগত তথ্য অন্য কারো সাথে শেয়ার করা থেকে বিরত থাকুন। আমাদের অ্যাপ্লিকেশনের মাধ্যমে কোন তথ্য আদান প্রদান করা যায় না।\n" +
                        "    \n" +
                        "    5. এই অ্যাপ্লিকেশন এ প্রায় সকল বিজ্ঞপ্তি প্রকাশ করার সাথে সু্ত্র ও তারিখ প্রদান করা হয়। তাই, আপনি একজন সচেতন নাগরিক হিসাবে বিজ্ঞপ্তির সুত্র যাচাই করে নিবেন।\n" +
                        "    \n" +
                        "    6. ইন্টারনেট এর জগতে হাজারো রকম অনৈতিক কার্যক্রম হয়ে থাকে। এমতাবস্থায় কোন ব্যক্তি আমাদের অ্যাপ্লিকেশন এর লোগো, ছবি, স্ক্রীনশট, লেখা, ফাইল দ্বারা কম্পিউটার ভাইরাস তৈরি সহ আদান প্রদান করে থাকলে সেটার দায়বদ্বতা আমাদের নয়।\n" +
                        "    \n" +
                        "    7. এই অ্যাপ্লিকেশন টি সবার জন্য ফ্রি। কোন প্রতিষ্ঠান বা ব্যক্তি এই অ্যাপ্লিকেশন টি ব্যবহার করার জন্য অর্থনৈতিক চাপ দেয় তাহলে সেটা গ্রহণ করবেন না।\n" +
                        "    \n" +
                        "    8. এই অ্যাপ্লিকেশন টি আপনার চারপাশের মানুষদের মাঝে শেয়ার করার অনুমতি রয়েছে।\n" +
                        "    \n" +
                        "    9. এই অ্যাপ্লিকেশন টির ভালো দিক বা খারাপ দিক নিয়ে কোন পরিচিত বা অপরিচিত মানুষের সাথে অসংযত, অযুক্তিগত কথা বলা থেকে বিরত থাকুন।\n" +
                        "    \n" +
                        "    10. আপনি আমাদের অ্যাপ্লিকেশন টির লেখা, ছবি সহ ইত্যাদি সবার সাথে শেয়ার করতে পারেন কিন্তু কোন ওয়েব সাইট, অ্যাপ্লিকেশন, ইমেইল সহ সকল মিডিয়াতে বিনা অনুমতিতে প্রকাশ করতে পারবেন না। যদি প্রকাশ করে থাকেন তাহলে অবশ্যই সূত্র প্রদান করতে হবে।\n" +
                        "    \n" +
                        "    11. বিজ্ঞপ্তি বা বিজ্ঞাপন প্রকাশ করতে ইচ্ছুক ব্যক্তি আমাদের সাথে যোগাযোগ করতে পারেন। সেক্ষেত্রে আমাদের শর্তাবলী মেনে বিজ্ঞপ্তি বা বিজ্ঞাপন দিতে হবে।\n" +
                        "    \n" +
                        "    12. উপরোক্ত সকল শর্তাবলী যে কোন সময়ে পরিবর্তন ও সংযোজন করার ক্ষমতা কর্তৃপক্ষ রাখে।";

                startActivity(new Intent(MainActivity.this, ActivityDocs.class).putExtra("message",conditions).putExtra("title",getString(R.string.title_trams_and_conditions)));
                break;
            case R.id.nav_menu_rate:
                Tools.rateAction(this);
                break;
        }
        actionBar.setTitle(title);
        drawer.closeDrawers();
        if (fragment != null) openFragment(fragment);
    }



    private void openFragment(Fragment fragment)   {
        clearStack();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();



    }

    public void clearStack() {
        //Here we are clearing back stack fragment entries
        int backStackEntry = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }

        //Here we are removing all the fragment that are shown here
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.colorTextAction));

      /*  final MenuItem menuItem = menu.findItem(R.id.action_search);
        View actionView = MenuItemCompat.getActionView(menuItem);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menu_id = item.getItemId();
        if (menu_id == android.R.id.home) {
            if (!drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.openDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        } else if (menu_id == R.id.action_search) {
            startActivity(new Intent(context,SearchActivity.class));
        } else if (menu_id == R.id.action_refresh) {
            Toast.makeText(context, "This app is underdevelopment", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    private void gettingApplicationInfo(){

    /*    Log.d(TAG, "gettingApplicationInfo: ");

        ProgressDialog dialog=new ProgressDialog(MainActivity.this);
        dialog.setMessage("Please wait");
        dialog.show();

        Call<AppInfo> getInfoCall=apiInterface.getApplicationInfo();
        getInfoCall.enqueue(new Callback<AppInfo>() {

            @Override
            public void onResponse(Call<AppInfo> call, Response<AppInfo> response) {
               Log.d(TAG, "onResponse: "+response.body().toString());

                if (response.isSuccessful() && response.body() !=null){
                    AppInfo appinfo=response.body();
                    SharedPref.putKey(context, Constants.APP_INFO,"yes");
                    SharedPref.putKey(context, Constants.APP_NAME,appinfo.getEBOOKAPP().get(0).getAppName());
                    SharedPref.putKey(context, Constants.APP_LOGO,appinfo.getEBOOKAPP().get(0).getAppLogo());
                    SharedPref.putKey(context, Constants.APP_VERSION,appinfo.getEBOOKAPP().get(0).getAppVersion());
                    SharedPref.putKey(context, Constants.APP_AUTHOR,appinfo.getEBOOKAPP().get(0).getAppAuthor());
                    SharedPref.putKey(context, Constants.APP_CONTACT,appinfo.getEBOOKAPP().get(0).getAppContact());
                    SharedPref.putKey(context, Constants.APP_EMAIL,appinfo.getEBOOKAPP().get(0).getAppEmail());
                    SharedPref.putKey(context, Constants.APP_WEBSITE,appinfo.getEBOOKAPP().get(0).getAppWebsite());
                    SharedPref.putKey(context, Constants.APP_DESCRIPTION,appinfo.getEBOOKAPP().get(0).getAppDescription());
                    SharedPref.putKey(context, Constants.APP_PRIVACY_POLICY,appinfo.getEBOOKAPP().get(0).getAppPrivacyPolicy());
                }else {
                    SharedPref.putKey(context, Constants.APP_INFO,"no");
                }



                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AppInfo> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Failed to retrive data", Toast.LENGTH_SHORT).show();
                SharedPref.putKey(context, Constants.APP_INFO,"no");
                dialog.dismiss();
            }
        });
*/
    }
}
