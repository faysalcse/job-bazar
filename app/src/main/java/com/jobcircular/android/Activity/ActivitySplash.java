package com.jobcircular.android.Activity;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jobcircular.android.ApiService.APIClient;
import com.jobcircular.android.ApiService.APIInterface;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.AppInfo;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Constants;
import com.jobcircular.android.Utils.SharedPref;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySplash extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    ProgressBar progressBar;

    public static final String TAG=ActivitySplash.class.getSimpleName();

    APIInterface apiInterface;
    Context context;

    OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=this;

        progressBar=findViewById(R.id.progressBar);

        apiInterface= APIClient.getClient().create(APIInterface.class);

        progressBar.setVisibility(View.GONE);




        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity


                progressBar.setVisibility(View.VISIBLE);

                if (App.isInternetOn(getApplicationContext())){
                    gettingApplicationInfo();
                }else {
                    connectionLostDialoge();
                }


                // close this activity
               // finish();
            }
        }, SPLASH_TIME_OUT);
    }





    private void connectionLostDialoge(){
        AlertDialog.Builder builder=new AlertDialog.Builder(ActivitySplash.this);
        builder.setMessage(getString(R.string.error_internet));
        builder.setCancelable(false);


        builder.setNegativeButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (App.isInternetOn(getApplicationContext())){
                    gettingApplicationInfo();
                }else {
                    connectionLostDialoge();
                }

            }
        });


        AlertDialog dialog=builder.create();
        dialog.show();

    }


    private void updateDialoge(){
        AlertDialog.Builder builder=new AlertDialog.Builder(ActivitySplash.this);
        builder.setTitle(getString(R.string.warning_message));
        builder.setMessage(getString(R.string.updateRequire));
        builder.setCancelable(false);


        builder.setPositiveButton(getString(R.string.update_app), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (App.isInternetOn(getApplicationContext())){
                    gettingApplicationInfo();
                }else {
                    connectionLostDialoge();
                }

            }
        });

        builder.setNegativeButton(getString(R.string.exit_app), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog dialog=builder.create();
        dialog.show();


    }


    private void gettingApplicationInfo(){

        Log.d(TAG, "gettingApplicationInfo: ");

        Call<AppInfo> getInfoCall=apiInterface.getApplicationInfo();
        getInfoCall.enqueue(new Callback<AppInfo>() {

            @Override
            public void onResponse(Call<AppInfo> call, Response<AppInfo> response) {
                Log.d(TAG, "onResponse: "+response.body().toString());

                if (response.isSuccessful() && response.body() !=null){
                    AppInfo appinfo=response.body();
                    SharedPref.putKey(context, Constants.APP_INFO,"yes");
                    SharedPref.putKey(context, Constants.APP_NAME,appinfo.getEBOOKAPP().get(0).getAppName());
                    SharedPref.putKey(context, Constants.APP_LOGO,appinfo.getEBOOKAPP().get(0).getAppLogo());
                    SharedPref.putKey(context, Constants.APP_VERSION,appinfo.getEBOOKAPP().get(0).getAppVersion());
                    SharedPref.putKey(context, Constants.APP_AUTHOR,appinfo.getEBOOKAPP().get(0).getAppAuthor());
                    SharedPref.putKey(context, Constants.APP_CONTACT,appinfo.getEBOOKAPP().get(0).getAppContact());
                    SharedPref.putKey(context, Constants.APP_EMAIL,appinfo.getEBOOKAPP().get(0).getAppEmail());
                    SharedPref.putKey(context, Constants.APP_WEBSITE,appinfo.getEBOOKAPP().get(0).getAppWebsite());
                    SharedPref.putKey(context, Constants.APP_DESCRIPTION,appinfo.getEBOOKAPP().get(0).getAppDescription());
                    SharedPref.putKey(context, Constants.APP_PRIVACY_POLICY,appinfo.getEBOOKAPP().get(0).getAppPrivacyPolicy());

                    if (getVersionCode() !=null){
                        if (!appinfo.getEBOOKAPP().get(0).getAppVersion().equals(getVersionCode())){
                            updateDialoge();
                        }else {
                            Intent i = new Intent(ActivitySplash.this, MainActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                           // finish();
                        }
                    }

                    Log.d(TAG, "Version of this app : "+getVersionCode());

                }else {
                    SharedPref.putKey(context, Constants.APP_INFO,"no");
                }



                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<AppInfo> call, Throwable t) {
                Log.d(TAG, "onFailure: failed to retrive data "+t.getMessage());
                Toast.makeText(context, "Failed to retrive data", Toast.LENGTH_SHORT).show();
                SharedPref.putKey(context, Constants.APP_INFO,"no");
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    public String getVersionCode(){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
