package com.jobcircular.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.widget.TextView;

public class ActivityDocs extends AppCompatActivity {

    String title;
    String message;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs);

        Intent intent=getIntent();
        if (intent !=null){
            title=intent.getStringExtra("title");
            message = intent.getStringExtra("message");
            key = intent.getStringExtra("key");
        }




        TextView titileOFDocs=findViewById(R.id.titileOFDocs);
        TextView descripitionOfDocs=findViewById(R.id.descripitionOfDocs);

        if (title !=null && !TextUtils.isEmpty(title)){
            titileOFDocs.setText(title);

        }

        if (message !=null && !TextUtils.isEmpty(message)){
            descripitionOfDocs.setText(Html.fromHtml(message));

        }
    }
}
